<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Session\Session;
use AdminBundle\Entity\Producto;
use AdminBundle\Entity\Categoria;
use WebBundle\Entity\Fileupload;
use Kreait\Firebase;
use GuzzleHttp\Client;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Google\Cloud\ServiceBuilder;
use Google\Cloud\Storage\StorageClient;
use Google\GitkitAccount;
use GuzzleHttp\ClientInterface;


class FirebaseController extends Controller {

  protected $locals      	 = array();
  protected $sessionVal  	 = array();
  protected $serviceAccount;
  protected $client;
  protected $google_api_url;
  protected $firebase;

  public function __construct(){
    $this->client         = new \GuzzleHttp\Client();
    $this->serviceAccount = ServiceAccount::fromJson(file_get_contents('../bodyguard4life-fa623-firebase-adminsdk-bs6q4-d6cea0e8b3.json'));
    $this->google_api_url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=';

    $this->firebase = (new Factory)
                                  ->withServiceAccount($this->serviceAccount)
                                  ->withDatabaseUri('https://bodyguard4life-fa623.firebaseio.com/')
                                  ->create();
  }


  public function findMembresiaUserAction($localid){

    try {
      $database  = $this->firebase->getDatabase();
      $reference = $database->getReference('/Kunden/'.$localid."/mitgliedSchaft");
      $snapshot = $reference->getSnapshot();
      $value = $snapshot->getValue();
      if (!$value) {
        throw new \Exception("there was an error, not data display");
      }
      $response['status'] = true;
      $response['data'] = $value;
    } catch (\Exception $e) {
      $response['status'] = false;
      $response['message'] = $e->getMessage();
    }
    return new JsonResponse($response);
  }


  public function existsMeineTraegermedien($user_uid){
    try {

      $database  = $this->firebase->getDatabase();
      $reference = $database->getReference('/Kunden/'.$user_uid."/meineTraegermedien");//////////esto coteja si meineTraegermedien,existe
      $exist     = $reference->getSnapshot()->exists();

      if (!$exist) {
        throw new \Exception("MeineTraegermedien does not exist");
      }
      $response['status']   = true;
    } catch (\Exception $e) {
      $response['status']   = false;
      $response['message']  = $e->getMessage();
    }

    return $response;
  }

  /**
   * @Route("/updatefb/", name="updatefb")
   */
  public function pushDataAction(Request $request){

    $session        = $request->getSession();
    $orderSummary   = $session->get('orderSummary');
    // dump($orderSummary);
    $status = '';
    $user_session = $session->get('user_session');
    // dump($user_session['currentPlan']);exit;
    $articles       = $session->get('articles');
    $tipoProducto   = 'armband';
    foreach ($articles as $key => $article) {
      if ($article['name'] !='armband') {
        $tipoProducto = $article['name'];
      }
    }
    $order['item']['itemName'] = $tipoProducto;
    $idpin        = $session->get('id_pin');
    $user_session = $session->get('user_session');
    $user_localId = $user_session['user']['uid'];
    $code         = $idpin['id'].$idpin['pin'];

    try {
      // $user_uid = '6PBRykue1RXkAhq7BXnFupVEthG2';
      // $exist_status = $this->existsMeineTraegermedien($user_localId);

      if ($user_session['currentPlan']['type'] === "gratis") {

        if ($tipoProducto !="armband") {
            $JSONresponse = $this->usedCodeWithidAction($code,$idpin,$user_localId);
            $respuesta = json_decode($JSONresponse->getcontent(), true);
            if (!$respuesta['status']) {
              throw new \Exception("there was an error,on firebase");
            }
            $this->mitgliedSchafPushAction($order,$user_localId,$status);
            $this->meineTraegermedienPushAction($order,$user_localId,$code,$idpin);

        }else {
            $JSONresponse = $this->usedCodeWithidAction($code,$idpin,$user_localId);
            $respuesta = json_decode($JSONresponse->getcontent(), true);
            if (!$respuesta['status']) {
              throw new \Exception("there was an error,on firebase");
            }
            $this->meineTraegermedienPushAction($order,$user_localId,$code,$idpin);
        }

      }else {
        if ($tipoProducto !="armband") {
          if ($user_session['currentPlan']['status'] === 'expired') {
            $status = $user_session['currentPlan']['status'];
            $this->mitgliedSchafPushAction($order,$user_localId,$status);
          }else {
            $this->mitgliedSchafPushUpgradeAction($order,$user_localId);
          }
        }
      }
      $response['status'] = true;
    } catch (\Exception $e) {
      $response['status'] = false;
      $response['message'] = $e->getMessage();
    }
    return new JsonResponse($response);
  }

  public function usedCodeWithidAction($code,$idpin,$user_localId) {

    try {
      $update = ["Id" => $idpin['id'],"Pin" => $idpin['pin'],"UserId" => $user_localId];
      $postData = [$code => $update ];
      $database  = $this->firebase->getDatabase();
      $database->getReference('/usedCodeWithID/')
                ->update($postData);/////////// esto solo guarda
      $reference = $database->getReference('/usedCodeWithID/'.$code);//////////esto coteja si la data fue ingresada, devuelve true
      $exist = $reference->getSnapshot()->exists();
      if (!$exist) {
        throw new \Exception("there was an error,on firebase");
      }
      $response['status'] =true;
    } catch (\Exception $e) {
      $response['status'] = false;
      $response['message'] = $e->getMessage();
    }
    return new JsonResponse($response);
  }

  public function mitgliedSchafPushAction($order,$user_localId,$status) {
    $database  = $this->firebase->getDatabase();
    $reference =  $database->getReference('/Kunden/'.$user_localId.'/mitgliedSchaft');
    $snapshot  = $reference->getSnapshot();
    $value     = $snapshot->getValue();
    $fechatermino = $value['fechaTermino'];
    $order_name   = $order['item']['itemName'];
    if ($status == 'expired') {
      $today     = date("D M d Y");
      $hoy       = strtotime($today)*1000;
      $new_fechatermino = $hoy + "31556926000";
    } else {
      $new_fechatermino = $fechatermino + "31556926000";
    }

    $update = [
                  'Kunden/'.$user_localId.'/mitgliedSchaft/Level' => ucfirst($order_name),
                  'Kunden/'.$user_localId.'/mitgliedSchaft/fechaTermino' => $new_fechatermino,
              ];
    $database->getReference()->update($update);
  }


  public function mitgliedSchafPushUpgradeAction($order,$user_localId) {
    $database  = $this->firebase->getDatabase();
    $reference =  $database->getReference('/Kunden/'.$user_localId.'/mitgliedSchaft');
    $snapshot  = $reference->getSnapshot();
    $value     = $snapshot->getValue();
    $fechatermino = $value['fechaTermino'];
    $order_name   = $order['item']['itemName'];
    $update = [
                  'Kunden/'.$user_localId.'/mitgliedSchaft/Level' => ucfirst($order_name),
              ];
    $database->getReference()->update($update);
  }

  public function meineTraegermedienPushAction($order,$user_localId,$code,$idpin){
      // dump($idpin);exit;

      $database  = $this->firebase->getDatabase();
      $reference =  $database->getReference('/Kunden/'.$user_localId.'/mitgliedSchaft');
      $snapshot  = $reference->getSnapshot();
      $value     = $snapshot->getValue();
      $fechatermino = $value['fechaTermino'];

      $reference1 = $database->getReference('/Kunden/'.$user_localId.'/meineTraegermedien');//////////esto coteja si la data fue ingresada, devuelve true
      $exist = $reference1->getSnapshot()->exists();
      $code = $idpin['id'].$idpin['pin'];
      if ($exist) {

        $update = ["Id" => $idpin['id'],"Pin" => $idpin['pin'],'Level'=> $order['item']['itemName'],'Status'=>'true','Time'=>$fechatermino];
        $postData = [$code => $update ];
        $database->getReference('/Kunden/'.$user_localId.'/meineTraegermedien/productos')
                  ->update($postData);/////////// esto solo guarda
        $update = [
                    'Kunden/'.$user_localId.'/meineTraegermedien/membresiaActual/Id' => $idpin['id'],
                    'Kunden/'.$user_localId.'/meineTraegermedien/membresiaActual/Pin' => $idpin['pin']
                  ];

        $database->getReference()->update($update);/////////// esto solo guarda
      }else {
        $update = ["Id" => $idpin['id'],"Pin" => $idpin['pin'],'Level'=> $order['item']['itemName'],'Status'=>'true','Time'=>$fechatermino];
        $update = [
                    'membresiaActual' => ['Id' => $idpin['id'],'Pin'=> $idpin['pin'] ],
                    'productos' => [$code => $update]
                  ];
        $postData = ['Kunden/'.$user_localId.'/meineTraegermedien' => $update ];
        $database  = $this->firebase->getDatabase();
        $database->getReference()
                  ->update($postData);/////////// esto solo guarda
      }

  }

  /**
   * @Route("/backOfficePush/", name="backOfficePush")
   */
  public function backOfficePushRefNumUsedAction(Request $request) {

    $startPoint   = 'BackOfficeData/RefNumUsed/';
    $session      = $request->getSession();
    $orderSummary = $session->get('orderSummary');
    $user_session = $session->get('user_session');
    $ref_Number    = empty($user_session['user']['nroReferencia']) ? 'DE0001' : $user_session['user']['nroReferencia'] ;
    $user_uid     = $user_session['user']['uid'];
    $numero_pais  = $this->buildRefNumber($ref_Number);
    $pais = $numero_pais['country'];
    $refNumber = $numero_pais['referenceNumber'];
    $today     = date("D M d Y");
    $hoy       = strtotime($today)*1000;

   try {

     $database  = $this->firebase->getDatabase();
     $new_enpoint = 'BackOfficeData/RefNumUsed/'.$pais.'/'.$refNumber.'/'.$user_uid;
     $exist = $this->exist($new_enpoint);  ///////////// SI existe el pais junto con el refNumber

     if (!$exist['status']) {
        $data    = ["x" => '1'];
        $udpate  = ['BackOfficeData/RefNumUsed/'.$pais.'/'.$refNumber.'/'.$user_uid => $data];
        $updated = $database->getReference()->update($udpate);
     }

     foreach ($orderSummary['items'] as $key => $value) {

       $topush = array('cantidad' => $value['quantity'],'Fecha' => $hoy,'Price' =>$value['price'], 'Producto' => $value['id'],'Key'=>'sdfsdfsdf' );
       $postRef = $database->getReference($new_enpoint)->push($topush);
       $postKey = $postRef->getKey();
       $this->updateId($postKey,$new_enpoint);
     }
     $this->mitgliedSchaftRefNUmberUpdate($user_uid,$refNumber,$ref_Number);

     $response['status'] = true;
   } catch (\Exception $e) {
     $response['status'] = false;
     $response['message'] = $e->getMessage();
   }

   return new JsonResponse($response);
 }


 public function updateId($postKey,$new_enpoint){
   try {
     $database = $this->firebase->getDatabase();
     $enpoint  = $new_enpoint.'/'.$postKey;
     $update   = array('Key' => $postKey);
     $database->getReference($enpoint)->update($update);
     $response['status']   = true;
   } catch (\Exception $e) {
     $response['status']   = false;
     $response['message']  = $e->getMessage();
   }
   return $response;
 }

 /**
  * @Route("/refNumber/", name="refNumber")
  */
 public function refNUmberExistAction($refNUmber) {

   try {

     if (empty($refNUmber)) {
       throw new \Exception("refNumber empty");
     }

     if ($refNUmber === 'DE0001') {
       throw new \Exception("DE0001 is a numero reservado");
     }
     $country = preg_replace('/[^A-z]/', '', $refNUmber);
     $refNumber = preg_replace('/[^0-9]/', '', $refNUmber);
     $database  = $this->firebase->getDatabase();
     $reference = $database->getReference('/BackOfficeData/RefNumCreated/'.$country);
     $snapshot  = $reference->getSnapshot();
     $value     = $snapshot->getValue();

     if (!$value) {
       throw new \Exception("This country does not exist!!");
     }

     if (!$value['Status']) {
       throw new \Exception("this country has been block!!");
     }
     $reference = $database->getReference('/BackOfficeData/RefNumCreated/'.$country.'/'.$refNumber);
     $snapshot  = $reference->getSnapshot();
     $value     = $snapshot->getValue();
     if (!$value['Status']) {
       throw new \Exception("this code has been block!!");
     }
     $reference = $database->getReference('/BackOfficeData/RefNumCreated/'.$country.'/'.$refNumber);
     $snapshot  = $reference->getSnapshot();
     $value     = $snapshot->getValue();
     $exist = $reference->getSnapshot()->exists();
     if (!$exist) {
       throw new \Exception("refNumber does not exist!!!");
     }
     $response['status'] = true;
   } catch (\Exception $e) {
     $response['status'] = false;
     $response['message'] = $e->getMessage();
   }

   return new JsonResponse($response);
 }


 public function exist($new_enpoint){
   try {

     $database = $this->firebase->getDatabase();
    //  $endpoint  = $point.'/'.$id;
     $reference = $database->getReference($new_enpoint);// verifica si data fue ingresada, devuelve true
     $exist     = $reference->getSnapshot()->exists();

      if (!$exist) {
        throw new \Exception("nothing found");
      }

      $response['status'] = true;

   } catch (\Exception $e) {
     $response['status']  = false;
     $response['message'] = $e->getMessage();
    }

   return $response;
 }


 public function buildRefNumber($refNUmber) {
   $respomse = [];
   $country = preg_replace('/[^A-z]/', '', $refNUmber);
   $refNumber = preg_replace('/[^0-9]/', '', $refNUmber);
   $response['country'] = $country;
   $response['referenceNumber'] = $refNumber;
   return $response;
 }


 public function mitgliedSchaftRefNUmberUpdate($user_uid , $nroReferencia,$pais_numeroRef) {
   $database  = $this->firebase->getDatabase();
   $reference = $database->getReference('/Kunden/'.$user_uid."/mitgliedSchaft/nroReferencia");
   $snapshot = $reference->getSnapshot();
   $value = $snapshot->getValue();
   if (empty($value)) {
     $database  = $this->firebase->getDatabase();
     $update   = array('nroReferencia' => $pais_numeroRef);
     $database->getReference('/Kunden/'.$user_uid."/mitgliedSchaft")->update($update);
    //  if ($nroReferencia !== 'DE0001' ) {
    //    $database  = $this->firebase->getDatabase();
    //    $update   = array('nroReferencia' => $pais_numeroRef);
    //    $database->getReference('/Kunden/'.$user_uid."/mitgliedSchaft")->update($update);
    //  } descomentar esto si se quiere guardar  "" por "DE0001"
   }
 }

}
