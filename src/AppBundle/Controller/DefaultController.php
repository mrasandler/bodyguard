<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use GuzzleHttp\Client;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class DefaultController extends Controller
{

  private $locals = array();
  private $serviceAccount;
  protected $request;

  public function __construct() {
    $this->serviceAccount = ServiceAccount::fromJson(
      file_get_contents('../bodyguard4life-fa623-firebase-adminsdk-bs6q4-d6cea0e8b3.json')
    );
  }

  /**
   * @Route("/login/", name="login4sos")
   */
  public function loginAction(Request $request) {
    $session = $request->getSession();

    if (!$session->has('user_session')) {
      return $this->render('default/login.html.twig');
    }else {
      return $this->redirect('/');
    }

  }

  /**
   * @Route("/pin/", name="error")
   */
  public function codigoPinAction(Request $request) {
    $id_pin = array();

    try {

      $session      = $request->getSession();
      $user_session = $session->get('user_session');
      $userUID =  $user_session['user']['uid'];

      if (!$userUID) {
        throw new \Exception("There is no session to get the user uid");
      }

      $characters   = $userUID;
      $charactersLength = strlen($characters);
      $id = '';
      $ping = '';

      for ($i = 0; $i < 8; $i++) {
        $id .= $characters[rand(0, $charactersLength - 1)];
      }

      for ($i = 0; $i < 10; $i++) {
        $ping .= $characters[rand(0, $charactersLength - 1)];
      }

      $id_pin['id'] = strtolower($id);
      $id_pin['pin'] = strtolower($ping);
      $response['status'] = true;
      $response['data'] = $id_pin;

    } catch (\Exception $e) {
      $response['status'] = false;
      $response['message']= $e->getMessage();
    }

    return new JsonResponse($response);
  }

  /**
   * @Route("/", name="index")
   */
  public function indexAction(Request $request) {
    $session     = $request->getSession();
    $userSession = $session->get('user_session');
    //  dump($session);exit;

    try {

      if (!$userSession) {
        return $this->redirect('/login/');
      }

      $memberships = $this->getMemberships();
      $membershipsExist = $memberships['status'];
      if (!$membershipsExist) {
        throw new \Exception($memberships['message']);
      }

      $response['status'] = true ;
      $response['currentPlan'] = $userSession['currentPlan'];
      $response['memberships'] = $memberships['memberships'];

    } catch (\Exception $e) {
      $this->addFlash('error', $e->getMessage() );
      return $this->redirect('/error/');
    }

   return $this->render('default/index.html.twig', $response);
  }

  /**
   * @Route("/cart/")
   * @Method({"GET","POST"})
   */
  public function cartAction(Request $request) {
    $session     = $request->getSession();
    $userSession = $session->get('user_session');
    $articles    = $request->get('articles');

    if (!$articles && $session->get('articles')) {
      $articles = $session->get('articles');
    }

    try {

      if (!$userSession) {
        return $this->redirect('/login/');
      }

      if (!$articles) {
        $response['status'] = false;
        return $this->render('default/cart.html.twig', $response);
      }

      // dump($articles);
      $session->set('articles', $articles);
      // dump($session->get('articles'));exit;
      $JSONresponse = $this->forward('AppBundle:Operaciones:createOrderSummary');

      $orderSummary = json_decode($JSONresponse->getcontent(), true);
      if (!$orderSummary['status']) {
        throw new \Exception($orderSummary['message']);
      }

      $order = $session->get('orderSummary');
      $response['status'] = true;
      $response['order'] = $order;
      $response['discountCode'] = $userSession['user']['nroReferencia'];

    } catch (\Exception $e) {
      $this->addFlash('error',$e->getMessage());
      return $this->redirect('/error/');
    }

    return $this->render('default/cart.html.twig', $response);
  }

  /**
   * @Route("/customer-information/")
   */
  public function customerInformationAction(Request $request) {

    $session      = $request->getSession();
    $articles     = $request->get('articles');
    $discountCode = $request->get('discountCode');

    $JSONresponse = $this->forward('AppBundle:Firebase:refNUmberExist',
      array('refNUmber' => $discountCode)
    );
    $response = json_decode($JSONresponse->getcontent(), true);
    if (!$response['status']) {
      $discountCode = "";
    }

    if (!$articles && $session->get('articles')) {
      $articles = $session->get('articles');
    }

    try {

      $session->set('articles', $articles);
      if ($discountCode) {
        $userSession = $session->get('user_session');
        $userSession['user']['nroReferencia'] = $discountCode;
        $session->set('user_session', $userSession);
        $session->set('discountCodeUsed', true);
      }

      $this->forward('AppBundle:Operaciones:createOrderSummary');
      $order = $session->get('orderSummary');
      $response['status'] = true;
      $response['order'] = $order;

    } catch (\Exception $e) {
      $this->addFlash('error',$e->getMessage());
      return $this->redirect('/error/');
    }

    //dump($response);exit;
    return $this->render('default/customerInformation.html.twig', $response);
  }

  /**
   * @Route("/payment/")
   * @Method("POST")
   */
   public function paymentAction(Request $request){
    $session       = $request->getSession();
    $customerData  = $request->request->all();

    try {

      $session->set('customerData', $customerData);
      $JSONresponse  = $this->forward('AppBundle:Operaciones:createPayment');
      //$JSONresponse2  = $this->forward('AppBundle:Operaciones:createOrderSummary');
      //dump($JSONresponse2);exit;
      // $orderSummary = json_decode($JSONresponse2->getcontent(), true);

      // if (!$orderSummary['status']) {
      //   throw new \Exception($orderSummary['message']);
      // }
      $payment  = json_decode($JSONresponse->getcontent(), true);

      if (!$payment['status']) {
        throw new \Exception($payment['message']);
      }

      $response['status'] = true;
      $response['order'] = $session->get('orderSummary');
      $response['payment'] = $payment['data'];
      $response['customerData'] = $session->get('customerData');

      //dump($response);exit;
    } catch (\Exception $e) {
      $this->addFlash('error',$e->getMessage());
      return $this->redirect('/error/');
    }

    return $this->render('default/payment.html.twig', $response);
  }

  /**
   * @Route("/payment/return/")
   * @Method("GET")
   */
  public function returnPaymentAction(Request $request){
    $session = $request->getSession();

    $session->set('payerID', $_GET['PayerID']);
    $session->set('token', $_GET['token']);

  try {

      $JSONresponse = $this->forward('AppBundle:Operaciones:ExecutePayment');
      $payment  = json_decode($JSONresponse->getcontent(), true);

      if($payment['status']) {

        $state = $payment['data']['state'];
        if (strtolower($state) === "approved") {
          if($session->get('discountCodeUsed')) $session->set('discountCodeUsed', false);

          $this->forward('AppBundle:Firebase:pushData', array('request' => $request));
          $this->forward('AppBundle:Firebase:backOfficePushRefNumUsed', array('request' => $request));
        // $response = json_decode($JSONresponse->getcontent(), true);
        //   if (!$response['status']) {
        //       throw new \Exception($response['message']);
        //   }
        }
        $this->addFlash('data',$payment['data']);
        }else {
          throw new \Exception($payment['message']);
        }

    } catch (\Exception $e) {
      //dump($e->getMessage());exit;
      $this->addFlash('error',$e->getMessage());
      return $this->redirect('/error/');
    }
    return $this->redirect('/payment/thankyou/');
  }

  /**
   * @Route("/payment/cancel/")
   * @Method("GET")
   */
  public function cancelPaymentAction(Request $request) {
    $session = $request->getSession();
    // $order = $session->get('orderSummary');
    //
    // $response['status'] = true;
    // $response['order'] = $order;
    // $response['discountCode'] = $userSession['user']['nroReferencia'];

    return $this->redirect('/cart/');
  }

  /**
   * @Route("/payment/thankyou/")
   * @Method("GET")
   */
  public function thankyouAction(Request $request) {
    $session      = $request->getSession();
    $user_session = $session->get('user_session');

    if (!$user_session) {
      return $this->redirect('/login/');
    }
    $session->invalidate();
    return $this->render('default/thankyou.html.twig');
  }

  /**
   * @Route("/error/")
   */
  public function errorAction() {
    return $this->render('exception/error.html.twig');
  }

  public function getMemberships() {

    try {

      $urlsite  =  $this->container->getParameter('fbdata');
      $firebase = (new Factory)
        ->withServiceAccount($this->serviceAccount)
        ->withDatabaseUri($urlsite)
        ->create();

      $database  = $firebase->getDatabase();
      $reference = $database->getReference('/membresia')->orderByChild('order')->startAt("0");
      $snapshot = $reference->getSnapshot();
      $values = $snapshot->getValue();
      // dump($values);exit;
      // foreach ($values as $key => $value) {
      //
      // }
      if (!$values) {
        throw new \Exception("there was an error, not data display");
      }

      $response['status']      = true;
      $response['memberships'] = $values;

    } catch (\Exception $e) {
      $response['status']  = false;
      $response['message'] = $e->getMessage();
    }

    return $response;
  }

  public function getCurrentPlan() {

  }

  /**
   * @Route("/check-discount-code/", name="check-discount-code")
   */
  public function checkDiscountCodeAction(Request $request) {
    $discountCode = $request->get('discountCode');
    $JSONresponse = $this->forward('AppBundle:Firebase:refNUmberExist',
      array('refNUmber' => $discountCode)
    );
    $response = json_decode($JSONresponse->getcontent(), true);

    return new JsonResponse($response);
  }


}
