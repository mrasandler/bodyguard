<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use GuzzleHttp\Client;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use PayPal\Exception\PayPalConnectionException;

// Define Sandbox or Live
define('PP_MODE', 'sandbox');
define('PP_URL', 'api.sandbox.paypal.com');

// Build API Credentials
DEFINE('PP_PROFILE_ID', 'XP-EU4S-LWCH-54GM-3MYH');
define('PP_CLIENTID', 'AYxesHUYWL39-sGqRQ1SnhrAHSi3yxlEYOek6M0b4WGuDSXODPoY2BZxBYSsf5ws5ZekaOsaKfhkUK8C');
define('PP_SECRET', 'EF0svIzWz7tj_WGDqpB58XettiQAuKKvf3fsikHmNC8wn6EdmOJkNI-FRr9vi9iJvixSFLlisgjbwd_p');
define('API_CREDENTIALS', PP_CLIENTID.':'.PP_SECRET);

define('CURRENCY_CODE', 'EUR');
define('LOCAL_CODE', 'de_DE');
define('TAX_RATE', '0.00');
define('SHIPPING_PRICE', '4.99');

define('URL_RETURN', '/payment/return?action=return');
define('URL_CANCEL', '/payment/cancel/');


class OperacionesController extends Controller {

  private $locals = array();
  private $serviceAccount;
  private $client;
  protected $urlsite;

  public function __construct(RequestStack $requestStack) {
    $this->client         = new \GuzzleHttp\Client();
    $this->request        = $requestStack->getCurrentRequest();
    $this->serviceAccount = ServiceAccount::fromJson(
      file_get_contents('../bodyguard4life-fa623-firebase-adminsdk-bs6q4-d6cea0e8b3.json')
    );
  }

  public function getMembershipData($idMembership) {
    $response['status'] = false;

    try {

      $fbDataUrl  =  $this->container->getParameter('fbdata');
      $firebase   = (new Factory)
        ->withServiceAccount($this->serviceAccount)
        ->withDatabaseUri($fbDataUrl)
        ->create();

      $database   = $firebase->getDatabase();
      $reference  = $database->getReference('/membresia/' . $idMembership);
      $snapshot   = $reference->getSnapshot();
      $value      = $snapshot->getValue();

      if (!$value) {
        throw new \Exception("Membership not found or unknow problem");
      }

      $response['status'] = true;
      $response['data'] = $value;

    } catch (\Exception $e) {
      $response['status']  = false;
      $response['message'] = $e->getMessage();
    }

    return $response;
  }

  /**
   * @Route("/remove-article/{idArticle}", name="remove-article")
   */
  public function removeArticle($idArticle) {
    $session      = $this->request->getSession();
    $userSession  = $session->get('user_session');
    $articles     = $session->get('articles');

    if ($userSession['currentPlan']['type'] == 'gratis') {

      if ($idArticle != 'armband') {
        foreach ($articles as $key => $article) {
          unset($articles[$key]);
        }
      } else {
        foreach ($articles as $key => $article) {
          if ($article['name'] == $idArticle) {
            unset($articles[$key]);
          }
        }
      }

    } else {
      foreach ($articles as $key => $article) {
        if ($article['name'] == $idArticle) {
          unset($articles[$key]);
        }
      }
    }

    $session->set('articles', $articles);
    //dump($articles, $idArticle, $session);exit;
    return $this->redirect('/cart/');
  }

  public function createOrderSummaryAction () {
    $session  = $this->request->getSession();
    $userSession = $session->get('user_session');
    $articles = $session->get('articles');
    $discountCodeUsed = $session->get('discountCodeUsed');

    try {

      //  obtiene el precio de la membresia a hacer upgrade
      $mtuName = "";
      foreach ($articles as $key => $article) {
        if ($article['name'] != 'armband') {
          $articleData = $this->getMembershipData($article['name']);
          $mtuPrice = $articleData['data']['price'];
          $mtuName = $articleData['data']['id'];
        }
      }

      // Obtiene el precio de la membresia actual
      $type = $userSession['currentPlan']['type'];
      if ($type != 'gratis')  {
        $data = $this->getMembershipData($type);
        $cmPrice = $data['data']['price'];
        $cmName = $data['data']['id'];
      } else {
        if(!$mtuName) $mtuName = 'test';
        $cmName = $mtuName;
      }

      if ($cmName != $mtuName && $userSession['currentPlan']['status'] != 'expired') {

        // solo si se hace upgrade
        foreach ($articles as $key => $article) {
          $articleData = $this->getMembershipData($article['name']);
          $membership  = $articleData['data'];

          if ($article['name'] != 'armband') {
            // enviar primero upgradeMembers, currentMember
            $actualPrice = $this->getPriceUpgrade($mtuPrice, $cmPrice);
          } else {
            $actualPrice = $membership['price'];
          }


          $itemList[$key] = [
            'id'  => $membership['id'],
            'quantity' => $article['quantity'],
            'name' => $membership['name'],
            'price' => number_format($actualPrice, 2),
            'image' => $membership['image'],
            'description' => $membership['description'],
          ];
        }


        $shippingPrice = $discountCodeUsed ? '0.00' : SHIPPING_PRICE;

        $subtotal = "0";
        foreach ($itemList as $item) {
          $subtotal += $item['price'] * $item['quantity'];
        }

        $shippingPrice = $discountCodeUsed ? '0.00' : SHIPPING_PRICE;

        $orderSummary = [
          'subtotal' => number_format($subtotal, 2),
          'shippingPrice' => $shippingPrice,
          'tax' => number_format($subtotal * TAX_RATE, 2),
          'total' => number_format($subtotal + $shippingPrice, 2),
          'currency' => CURRENCY_CODE,
          'items'=> $itemList
        ];

      } else {

        foreach ($articles as $key => $article) {
          $articleData = $this->getMembershipData($article['name']);
          $membership  = $articleData['data'];

          $itemList[$key] = [
            'id'  => $membership['id'],
            'quantity' => $article['quantity'],
            'name' => $membership['name'],
            'price' => number_format($membership['price'], 2),
            'image' => $membership['image'],
            'description' => $membership['description'],
          ];
        }

        $subtotal = "0";
        foreach ($itemList as $item) {
          $subtotal += $item['price'] * $item['quantity'];
        }

        $shippingPrice = $discountCodeUsed ? '0.00' : SHIPPING_PRICE;

        $orderSummary = [
          'subtotal' => number_format($subtotal, 2),
          'shippingPrice' => $shippingPrice,
          'tax' => number_format($subtotal * TAX_RATE, 2),
          'total' => number_format($subtotal + $shippingPrice, 2),
          'currency' => CURRENCY_CODE,
          'items'=> $itemList
        ];

      }


      $response['status'] = true;
      $session->set('orderSummary', $orderSummary);

      // dump($orderSummary);exit;

    } catch (\Exception $e) {
      $response['status']  = false;
      $response['message'] = $e->getMessage();
    }

    return new JsonResponse($response);
  }

  public function getPriceUpgrade($upgradeMembership,$currentMembership) {
    $ml = $this->getMonthsLeft();
    return (($upgradeMembership-$currentMembership) / 12)*$ml;
  }

  public function getMonthsLeft() {
    $session       = $this->request->getSession();
    $user_session  = $session->get('user_session');
    $endDate       = $user_session['currentPlan']['endDate1'];

    $today = date("Y-m-d");

    $diff = abs(strtotime($today) - strtotime($endDate));

    $years = floor($diff / (365*60*60*24));
    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

    $suma = $years*12 + $months + (floor($days/31));

    return $suma;
  }

  /**
   * @Route("/token/")
   */
  public function getAccesTokenAction () {
    $endpoint = 'https://'.PP_URL.'/v1/oauth2/token?grant_type=client_credentials';

    try {

      $parameters = [
        'headers' => [
          'Content-Type'    => 'application/x-www-form-urlencoded',
          'Accept'          => 'application/json',
          'Accept-Language' => 'en_Us',
          'Authorization'   => 'Basic QVl4ZXNIVVlXTDM5LXNHcVJRMVNuaHJBSFNpM3l4bEVZT2VrNk0wYjRXR3VEU1hPRFBvWTJCWnhCWVNzZjV3czVaZWthT3NhS2Zoa1VLOEM6RUYwc3ZJeld6N3RqX1dHRHFwQjU4WGV0dGlRQXVLS3ZmM2ZzaWtIbU5DOHduNkVkbU9Ka05JLUZScjl2aTlpSnZpeFNGTGxpc2dqYndkX3A=',
        ],
        'postfields' => 'gran_type=client_credentials&='
      ];

      $JSONresponse = $this->client->post($endpoint,$parameters);
      $content = $JSONresponse->getBody()->getContents();

      if ($content) {
        $data                 =  json_decode($content,true);
        $response['status']  = true;
        $response['data']    =  $data;
      }

    } catch (\Exception $e) {
      $response['status']  = false;
      $response['message'] = $e->getMessage();
    }

    return $response;
  }

  /**
   * @Route("/payment/create/")
   */
  public function createPaymentAction(Request $request) {
    $response['status']  = false;
    $session = $request->getSession();
    $customerData = $session->get('customerData');

    try {

     $userSession  = $session->get('user_session');
     $currentPlan  = strtolower($userSession['currentPlan']['type']);
     $pinID        = "";

     if ($currentPlan == 'gratis') {
       $JSONresponse  = $this->forward('AppBundle:Default:codigoPin', array('request' => $request));
       $codigoPin     = json_decode($JSONresponse->getcontent(),true);

       if (!$codigoPin['status']) {
         throw new \Exception($codigoPin['message']);
       }

       $session->set('id_pin', $codigoPin['data']);
       $pinID  = "Id: ". $codigoPin['data']['id'] .", Pin: ". $codigoPin['data']['pin'];
     }

     $ur_return = $this->container->getParameter('urlsite').URL_RETURN;
     $ur_cancel = $this->container->getParameter('urlsite').URL_CANCEL;

     $JSONresponse  = $this->createOrderSummaryAction();
     //$content  = json_decode($JSONresponse->getcontent(), true);
     $order    = $session->get('orderSummary');
     //dump($order);exit;

     $items = array();
     foreach ($order['items'] as $key => $item) {
       $items[] = array(
         "quantity"  => $item['quantity'],
         "name"      => $item['name'],
         "price"     => $item['price'],
         "currency"  => CURRENCY_CODE
       );
     }
     $json = json_encode($items);

     //$session->set('order', $order);
     //$session->set('id_pin', $codigoPin['data']);

     $JSONrequest = '{
       "intent": "sale",
       "payer": { "payment_method": "paypal" },
       "transactions": [{
         "amount": {
           "currency": "'.CURRENCY_CODE.'",
           "total": "'.$order['total'].'",
           "details": {
             "subtotal": "'.$order['subtotal'].'",
             "tax": "'.$order['tax'].'",
             "shipping": "'.$order['shippingPrice'].'"
           }
         },
         "description": "'.$pinID.'",
         "item_list": {
           "items": '. $json .',
           "shipping_address": {
             "recipient_name": "'.$customerData['firstName'].' '.$customerData['lastName'].'",
             "line1": "'.$customerData['address1'].'",
             "line2": "'.$customerData['address2'].'",
             "city": "'.$customerData['city'].'",
             "state": "'.$customerData['state'].'",
             "postal_code": "'.$customerData['zip'].'",
             "country_code": "'.$customerData['country'].'",
             "phone": "'.$customerData['buyerPhone'].'"
           }
         }
       }],
       "redirect_urls": {
         "return_url": "'.$ur_return.'",
         "cancel_url":  "'.$ur_cancel.'"
       },
       "experience_profile_id":"'.PP_PROFILE_ID.'"
     }';

     $accestoken = $this->getAccesTokenAction();

     if ($accestoken['status'] == false) {
       throw new \Exception($accestoken['message']);
     }

     $bearer_token = $accestoken['data']['token_type'] .' ' . $accestoken['data']['access_token'];
     $endpoint     = 'https://'.PP_URL.'/v1/payments/payment';
     $parameters   = [
       'headers' =>  [
         'Content-Type'  => 'application/json',
         'Authorization' => $bearer_token
       ],
       'json' => json_decode($JSONrequest)
     ];

     $JSONresponse = $this->client->post($endpoint,$parameters);
     $content      = $JSONresponse->getBody()->getContents();

     if ($content) {

       $data               = json_decode($content,true);
       $response['status'] = true;
       $response['data']   = $data;

       $session = new Session();
       $session->set('approvalURL', $data['links'][1]['href']);
       $session->set('redirectURL', $data['links'][2]['href']);
       $session->set('paymentID', $data['id']);

     } else {
       throw new \Exception('Error');
     }

    } catch (\Exception $e) {
      $response['status'] = false;
      $response['message'] = $e->getMessage();
    }
    // dump($response);exit;
    return new JsonResponse($response);
  }

  /**
   * @Route("/payment/execute/")
   * @Method({"GET", "POST"})
   */
  public function ExecutePaymentAction(Request $request){
    $session      = $request->getSession();
    $payerID      = $session->get('payerID');
    $paymentID    = $session->get('paymentID');
    $customerData = $session->get('customerData');
    //dump($session);exit;
    //$memberships = $session->get('memberships');

    try {

      $JSONresponse  = $this->createOrderSummaryAction();
      //$content = json_decode($JSONresponse->getcontent(), true);
      $order    = $session->get('orderSummary');

      $items = array();
      foreach ($order['items'] as $key => $item) {
        $items[] = array(
          "quantity"  => $item['quantity'],
          "name"      => $item['name'],
          "price"     => $item['price'],
          "currency"  => CURRENCY_CODE
        );
      }

      $json = json_encode($items);

      $JSONrequest = '{
        "payer_id":"'.$payerID.'",
        "transactions": [{
          "amount": {
            "currency": "'.CURRENCY_CODE.'",
            "total": "'.$order['total'].'",
            "details": {
              "subtotal": "'.$order['subtotal'].'",
              "tax": "'.$order['tax'].'",
              "shipping": "'.$order['shippingPrice'].'"
            }
          },
          "item_list": {
            "items": '. $json .',
            ],
            "shipping_address": {
              "recipient_name": "'.$customerData['firstName'].' '.$customerData['lastName'].'",
              "line1": "'.$customerData['address1'].'",
              "line2": "'.$customerData['address2'].'",
              "city": "'.$customerData['city'].'",
              "state": "'.$customerData['state'].'",
              "postal_code": "'.$customerData['zip'].'",
              "country_code": "'.$customerData['country'].'",
              "phone": "'.$customerData['buyerPhone'].'"
            }
          }
        }]
      }';

      $accestoken = $this->getAccesTokenAction();

      if ($accestoken['status'] == false) {
        throw new \Exception($accestoken['message']);
      }

      $endpoint     = 'https://'.PP_URL.'/v1/payments/payment/'.$paymentID.'/execute';
      $bearer_token = $accestoken['data']['token_type'].' ' . $accestoken['data']['access_token'] ;
      $parameters   = [
        'headers' => [
          'Content-Type'  => 'application/json',
          'Authorization' => $bearer_token
        ],
        'json'    => ['payer_id' => $payerID]
      ];

      $JSONresponse = $this->client->post($endpoint,$parameters);
      $content      = $JSONresponse->getBody()->getContents();

      if ($content) {
        $data                 =  json_decode($content,true);
        $response['status']  = true;
        $response['data']    =  $data;
      } else {
        throw new \Exception('Error');
      }

    } catch (\Exception $e) {
      $response['status'] = false;
      $response['message'] = $e->getMessage();
      //dump($e->getMessage());exit;
    }

    return new JsonResponse($response);
  }

   /**
   * @Route("/payment/cancel/")
   * @Method({"GET", "POST"})
   */
  public function cancelPaymentAction(Request $request) {

  }

}
