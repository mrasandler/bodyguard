<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Session\Session;
use AdminBundle\Entity\Producto;
use AdminBundle\Entity\Categoria;
use WebBundle\Entity\Fileupload;
use Kreait\Firebase;
use GuzzleHttp\Client;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Google\Cloud\ServiceBuilder;
use Google\Cloud\Storage\StorageClient;
use Google\GitkitAccount;
use GuzzleHttp\ClientInterface;


class LoginController extends Controller {

  protected $locals      	 = array();
  protected $sessionVal  	 = array();
  protected $DEFAULT_TOKEN = "AIzaSyCdYGpL6w4XMlWQYc6Pj1t3DcXdfpZjnu8";
  protected $DEFAULT_URL 	 = "https://todolist-d8964.firebaseio.com";
  protected $serviceAccount;
  protected $endpoint_pass = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/setAccountInfo?key=AIzaSyCcUfbiaTNT_4O-sUErd1ET1n197GDOTS4';
  protected $endpoint_reset= 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/resetPassword?key=AIzaSyCcUfbiaTNT_4O-sUErd1ET1n197GDOTS4';
  protected $client;
  protected $google_api_url;


  public function __construct(){
    $this->client         = new \GuzzleHttp\Client();
    $this->serviceAccount = ServiceAccount::fromJson(file_get_contents('../bodyguard4life-fa623-firebase-adminsdk-bs6q4-d6cea0e8b3.json'));
    $this->google_api_url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=';
  }
  /**
   * @Route("/seguridad/", name="seguridad")
   * @Method({"GET", "POST"})
   */

   public function seguridadAction(Request $request){
     $session            = $request->getSession();
     $urlsite            =  $this->container->getParameter('urlsite');
     $WepApikey          = $this->container->getParameter('WepApikey');
     $endpoint           = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key='.$WepApikey;
     $response           = array();
     $message['mensaje'] = '';
     $parameters         = $request->request->all();

          if ($parameters) {

            $email      = $parameters['email'];
            $pass   = $parameters['pass'];

        	}else {

          	$email = '';
          	$pass  = '';
        	}

        	$user_session = $session->get('user_session');
        	try {

              	if (!$user_session) {
                  // dump("algo se cruzo ");exit;
                  //// si no existe ninguna session, creara una,
                  //// si y solo si exista un usuario y contraseña y despues te manda a tu vista determinada
                  if (strlen($email) and  strlen($pass)) {                	/// si es q mandan un usuario y contraseña, busca en firebase y logea al usuario
                   	  $payload  	= ['email' => $email ,'password' => $pass,'returnSecureToken' => true];
                      $parameters = ['headers' => ['Content-Type' => 'application/json'] ,'json' => $payload];
                      $post       = $this->client->post($endpoint,$parameters);
                      $res        = $post->getBody()->getContents();
                      $json_res   = json_decode($res,true);
                      if ($json_res['registered']== true) {
                        $JSONresponse  = $this->forward('AppBundle:Firebase:findMembresiaUser', array(
                          'localid' => $json_res['localId']
                        ));

                        $mitgliedschaft = json_decode($JSONresponse->getcontent(),true);
                        // dump($mitgliedschaft['data']);exit;
                        $fechaCreacionSeconds = $mitgliedschaft['data']['fechaCreacion'] / 1000;
                        $fechaTerminoSeconds  = $mitgliedschaft['data']['fechaTermino']  / 1000;
                        $status    = '';
                        $endDate1  = date("Y-m-d", $fechaTerminoSeconds);
                        $startDate = date("D M d Y", $fechaCreacionSeconds);
                        $endDate   = date("D M d Y", $fechaTerminoSeconds);
                        $today     = date("D M d Y");
                        $hoy       = strtotime($today)*1000;
                        if ($hoy >= $mitgliedschaft['data']['fechaTermino']  ) {
                          $status = 'expired';
                        }
                        $arrayUser = array(
                          'user'  => array(
                            'uid'   => $json_res['localId'],
                            'email' => $json_res['email'],
                            'nroReferencia' => $mitgliedschaft['data']['nroReferencia']
                          ),
                          'currentPlan' => array(
                            'type'  =>  strtolower($mitgliedschaft['data']['Level']),
                            'sizeStorage' => $mitgliedschaft['data']['freeSpace'],
                            'startDate'   => $startDate,
                            'endDate1'    => $endDate1,
                            'endDate'     => $endDate,
                            'status'      => $status
                            )
                          );

                        $session->set('user_session',$arrayUser);
                        $response['status'] = true;
                        $response['data'] = $urlsite;
                      }
                	}
              	}

        	} catch (\Exception $e) {

              $response = array();
              $var      = $e->getMessage();

              if (stripos($var,"EMAIL_NOT_FOUND")) {
                $response['status'] = false;
                // $response['error']  = "EMAIL_NOT_FOUND";
                $response['error']  = "Die E-Mail-Adresse oder Passwort wurde falsch angegeben.";
              } elseif (stripos($var,"INVALID_PASSWORD")) {
                $response['status'] = false;
                // $response['error']  = "INVALID_PASSWORD";
                $response['error']  = "Die E-Mail-Adresse oder Passwort wurde falsch angegeben.";
              } elseif (stripos($var,"INVALID_EMAIL")) {
                $response['status'] = false;
                // $response['error']  = "INVALID_PASSWORD";
                $response['error']  = "Die E-Mail-Adresse oder Passwort wurde falsch angegeben.";
              }elseif (stripos($var,"TOO_MANY_ATTEMPTS_TRY")) {
                $response['status'] = false;
                $response['error']  = "TOO MANY ATTEMPTS, TRY LATER";
              }else{
                      if($var != ''){

                        $response['status'] = false;
                        $response['error']  = $var;
                      }else{
                        $response['status'] = false;
                        $response['error']  = "There was a problem, try again later!";
                      }
              }

               return new JsonResponse($response);
            //  return $this->render('default/login.html.twig',$response);
          }

         return new JsonResponse($response);
  }


   /**
	* @Route("/logout/", name="logout")
	* @Method({"GET", "POST"})
	*/

   public function logoutAction(Request $request){
     //$this->push_fb_vistas();
      $session = $request->getSession();
      //  $session->remove('res');
      //  dump("limpia");exit;
      //  $session->invalidate();
      $session->invalidate();
      $urlsite =  $this->container->getParameter('urlsite');
      $url = "/login";
      // dump($fbDataUrl.$url);exit;
      return $this->redirect($url);
   }

   public function getMembershipData(){

     $response['status'] = false;

     try {

       $fbDataUrl =  $this->container->getParameter('fbdata');

       $firebase = (new Factory)
         ->withServiceAccount($this->serviceAccount)
         ->withDatabaseUri($fbDataUrl)
         ->create();

       $database  = $firebase->getDatabase();
       $reference = $database->getReference('/membresia');
       $snapshot = $reference->getSnapshot();
       $value = $snapshot->getValue();

       if (!$value) {
         throw new \Exception("membership not found or unknow problem");
       }

       $response['status'] = true;
       $response['memberships'] = $value;

     } catch (\Exception $e) {
       $response['status']  = false;
       $response['message'] = $e->getMessage();
     }

     return $response;
   }

	function buscarUsuario($localId){

  	// $this->serviceAccount = ServiceAccount::fromJson(file_get_contents('../todolist-d8964-firebase-adminsdk-gcaa6-32d63eef22.json'));
    try {

      $service2  = $this->container->get('service_WebBundle');

      $rfirebase  = $service2->get_firebase_sdk('');
      if($rfirebase['status'] == false){
         throw new \Exception($rfirebase['message']);
      }

      $firebase  = $rfirebase['firebase'];

    	$database = $firebase->getDatabase();

    	$reference = $database->getReference('/userWulff')->orderByChild('id_uid')->equalTo($localId);
    	$snapshot = $reference->getSnapshot();

      $response['status'] = true;
      $response['data']   = $snapshot->getValue();

    } catch (\Exception $e) {

      $response['status']   = false;
      $response['message']  = $e->getMessage();
    }


  	return $response;
	}

}
