var order;
var productName = $('#articleName-js').text().toLowerCase();

// Set up urls to create and execute the payment
var CREATE_PAYMENT_URL = '/payment/create/' + productName; //'/demo/checkout/api/paypal/payment/create/';
 //'/demo/checkout/api/paypal/payment/execute/';

paypal.Button.render({

  env: 'sandbox',
  commit: true,
  style: {
    label: 'paypal',
    size: 'responsive',
    shape: 'rect',
    color: 'gold'
  },

  // payment() is called when the button is clicked
  payment: function () {

    // Make a call to server to set up the payment
    return paypal.request.post(CREATE_PAYMENT_URL)
      .then(function (res) {
        return res.data.id;
      });
  },

  // onAuthorize() is called when the buyer approves the payment
  onAuthorize: function (data, actions) {

    // Set up the data to pass to server
    var data = {
      paymentID: data.paymentID,
      payerID: data.payerID
    };

    console.log('Entre authorize: ' + data);

    var EXECUTE_PAYMENT_URL = '/payment/execute';

    // Make a call to execute the payment
    return paypal.request.post(EXECUTE_PAYMENT_URL, data)
    .then(function (res) {
        window.alert('Payment Complete!');
    });
  },

  onCancel: function (data, actions) {
    console.log('Payment Cancelled!');
  },

  onError: function (err) {
    console.log('There was an error: \n' + err);
  },

}, '#paypal-button');
