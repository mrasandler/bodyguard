(
  // Add class active to current membership
  function addActiveClassOnCurrentMembership() {
    // var membership = $('#currentPlan').val();
    // membership = membership == 'Gratis' ? 'bronze' : membership;
    // var currentMembership = $('[data-id="'+ membership +'"]');

    // loadMembershipData(currentMembership);
    // currentMembership.addClass('active');
    addDisableClass();
  }

)();

$('.membershipCard-js').on('click', function () {
  var planStatus = $('#planStatus').val() || 'active';
  var currentPlan = $('#currentPlan').val();
  var memberships = $('.membershipCard-js');
  var currentMembershipSelected = $(this);

  if (planStatus !== 'expired' && currentPlan !== 'gratis') {

    if (currentMembershipSelected.hasClass('active')) {
      currentMembershipSelected.removeClass('active');
      removeMembershipData();
    } else {
      memberships.removeClass('active');
      currentMembershipSelected.addClass('active');
      loadMembershipData(currentMembershipSelected);
    }

  } else {
    console.log('entre al else');

    if (currentMembershipSelected.hasClass('active')) {
      currentMembershipSelected.removeClass('active');
    }

    memberships.removeClass('active');
    currentMembershipSelected.addClass('active');
    loadMembershipData(currentMembershipSelected);
  }

  return false;
});

function loadMembershipData (Membership) {
  var membership = $('.membershipDataContent');
  var id = Membership.attr('data-id');
  var name = Membership.attr('data-name');
  var description = Membership.attr('data-description');
  var descriptionOne = Membership.attr('data-description-one');

  var data =
    '<input type="hidden" name="articles[0][name]" id="membershipId" value="'+ id +'">' +
    '<input type="hidden" name="articles[0][quantity]" value="1">'+
    '<h1 class="product-title" id="membershipName">'+ name +'</h1>' +
    '<p class="product-description" id="membershipDescription">'+ description +'</p>' +
    '<p class="product-description mt-2" id="membershipDescriptionOne">'+ descriptionOne +'</p>';

    membership.html(data);
}

function removeMembershipData () {
  var content = $('.membershipDataContent');
  content.html('');
}

$('.armbandCard-js').on('click', function () {
  var currentArmbandSelected = $(this);

  if (currentArmbandSelected.hasClass('active')) {
    currentArmbandSelected.removeClass('active');
    removeArmbandData();
  } else {
    currentArmbandSelected.addClass('active');
    loadArmbandData(currentArmbandSelected);
  }

  return false;
});

function loadArmbandData (Armband) {
  var armband = $('.armbandDataContent');
  var id = Armband.attr('data-id');
  var name = Armband.attr('data-name');
  var description = Armband.attr('data-description');

  var data =
    '<input type="hidden" name="articles[1][name]" id="armbandId" value="'+ id +'">' +
    '<input type="hidden" name="articles[1][quantity]" value="1">'+
    '<h1 class="product-title" id="armbandName">'+ name +'</h1>' +
    '<p class="product-description" id="armbandDescription">'+ description +'</p>';

  armband.html(data);
}

function removeArmbandData () {
  var content = $('.armbandDataContent');
  content.html('');
}

function addDisableClass () {
  var currentPlan = $('#currentPlan').val();
  //currentPlan = currentPlan == 'Gratis' ? 'bronze' : currentPlan;
  var planStatus = $('#planStatus').val();
  var bronze = $('[data-id="bronze"');
  var silber = $('[data-id="silber"');
  var gold = $('[data-id="gold"');

  switch(currentPlan) {

    case 'bronze':
      if (planStatus != 'expired') {
        bronze.attr('disabled', true);
        loadMembershipData(silber);
        silber.addClass('active');
      } else {
        bronze.addClass('active');
        loadMembershipData(bronze);
      }

      break;
    case 'silber':
      if (planStatus != 'expired') {
        bronze.attr('disabled', true);
        silber.attr('disabled', true);
        loadMembershipData(gold);
        gold.addClass('active');
      } else {
        bronze.attr('disabled', true);
        silber.addClass('active');
        loadMembershipData(silber);
      }

      break;
    case 'gold':
      if (planStatus != 'expired') {
        bronze.attr('disabled', true);
        silber.attr('disabled', true);
        gold.attr('disabled', true);
        removeMembershipData();
      } else {
        bronze.attr('disabled', true);
        silber.attr('disabled', true);
        gold.addClass('active');
        loadMembershipData(gold);
      }

      break;

    default:
      bronze.attr('disabled', false);
      silber.attr('disabled', false);
      gold.attr('disabled', false);
      loadMembershipData(bronze);
      bronze.addClass('active');
  }

}
