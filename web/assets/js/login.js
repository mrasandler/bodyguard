/********* VALIDATIONS *********/

// Configure jQuery Validation
$.validator.setDefaults({
  errorElement: "div",
  errorClass: 'invalid-feedback',
  highlight: function (element) {
    $(element).addClass('is-invalid').removeClass('is-valid');
  },
  unhighlight: function (element) {
    $(element).removeClass('is-invalid').addClass('is-valid');
  }
});

$('#loginForm').validate({
  submitHandler: function(form) {
    var data = $(form).serialize();
    var btn  = $(form).find(':submit');

    $.ajax({
      url: form.action,
      type: form.method,
      dataType: 'json',
      data: data,

      beforeSend: function () {
        btn.addClass('spinner disabled');
      },

      success: function (response) {
        if (response.status) {
          window.location.replace('/');
        } else {
          swal({
            type: 'error',
            title: 'Oops...',
            text: response.error,
            showConfirmButton: false,
            timer: 2500
          });
        }
      },

      complete: function () {
        btn.removeClass('spinner disabled');
      }

    });

   }
});
