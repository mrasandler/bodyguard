var armbandQuantity = $('#armbandQuantity').val() || "0";

$('#armbandQuantity').on('change', function () {
  armbandQuantity = $(this).find(':selected').val();
  updateOrderTotal();
});

$('#checkDiscountCode').on('click', function () {
  var btn = $('#checkDiscountCode');
  var input = $('#discountCode');
  var helpBlock = $('#discountCodeHelpBlock');
  var discountCode = $('#discountCode').serialize();
  var icon = '<svg width="21" height="21" viewBox="0 0 24 24" fill="none" stroke="#3ad994" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>';

  swal({
    title: 'Möchten Sie diese Referenznummer wirklich verwenden?',
    text: "Sie können diesen Vorgang nicht rückgängig machen!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'ja, mache ich!',
    cancelButtonText: 'Nein!'
  }).then((result) => {
    if (result.value) {

      $.ajax({
        url: '/check-discount-code/',
        type: 'post',
        dataType: 'json',
        data: discountCode,

        beforeSend: function () {
          btn.addClass('spinner disabled');
        },

        success: function (response) {
          if (response.status) {
            helpBlock.fadeOut();
            input.prop('readonly', true);
            btn.prop('disabled', true);
            btn.addClass('d-flex');
            btn.html(icon);
            discountShippingPrice();
            updateOrderTotal();
          } else {
            helpBlock.fadeIn();
          }
        },

        complete: function () {
          btn.removeClass('spinner disabled');
        }

      });

    } else {
      input.val('');
    }
  });

});

function discountShippingPrice () {
  $('#orderShippingPrice').text('0.00');
}

function updateOrderTotal () {
  // Articles
  var armbandPrice = $('#armbandPrice').val() || "0";
  var membershipPrice = $('#membershipPrice').val() || "0";
  var membershipQuantity = $('#membershipQuantity').val() || "0";
  var armbandSubtotal = parseFloat(armbandPrice) * parseInt(armbandQuantity);
  var membershipSubtotal = parseFloat(membershipPrice) * parseInt(membershipQuantity);
  console.log(armbandQuantity, membershipSubtotal);

  // Order
  var itemsQuantity = $('#itemsQuantity');
  var orderSubtotal = $('#orderSubtotal');
  var orderShippingPrice = $('#orderShippingPrice').text();
  var orderTotal = $('#orderTotal');

  itemsQuantity.text(parseInt(membershipQuantity) + parseInt(armbandQuantity));
  orderSubtotal.text((membershipSubtotal + armbandSubtotal).toFixed(2));
  orderTotal.text((membershipSubtotal + armbandSubtotal + parseFloat(orderShippingPrice)).toFixed(2));
}
